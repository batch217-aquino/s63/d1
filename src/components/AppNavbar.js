import { useContext } from "react"
import { NavLink } from "react-router-dom";
import { Container, Nav, Navbar } from "react-bootstrap";
import UserContext from "../UserContext";

export default function AppNavbar(){
    let userInfo = {
        token: localStorage.getItem("token"),
        id: localStorage.getItem("id"),
        isAdmin: localStorage.getItem("isAdmin")
      }

    const { user } = useContext(UserContext);
    console.log(user);

    if(userInfo.token === null && userInfo.id === null && userInfo.isAdmin === null ){
        return(
            <Navbar bg="dark" expand="lg" variant="dark" className="fixed-top">
                <Container fluid>
                    <Navbar.Brand as={ NavLink } to="/">TECHSHOP</Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="ms-auto">
                        <Nav.Link as={ NavLink } to="/" end>Home</Nav.Link>
                        <Nav.Link as={ NavLink } to="/login" end>Login</Nav.Link>
                        <Nav.Link as={ NavLink } to="/register" end>Register</Nav.Link>
                    </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        )
    }
    
    else if(userInfo.isAdmin === "true"){
        return (
            <Navbar bg="dark" expand="lg" variant="dark" className="fixed-top">
                <Container fluid>
                    <Navbar.Brand as={ NavLink } to="/">TECHSHOP - ADMIN</Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="ms-auto">
                        <Nav.Link as={ NavLink } to="/" end>Home</Nav.Link>
                        <Nav.Link as={ NavLink } to="/products/all" end> Dashboard</Nav.Link>
                        <Nav.Link as={ NavLink } to="/logout" end>Logout</Nav.Link>
                    </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        )
    }


    else {
        return (
            <Navbar bg="dark" expand="lg" variant="dark" className="fixed-top">
            <Container fluid>
            <Navbar.Brand as={ NavLink } to="/">TECHSHOP</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="ms-auto">
              <Nav.Link as={ NavLink } to="/" end>Home</Nav.Link>
              <Nav.Link as={ NavLink } to="/products" end>Products</Nav.Link>
              <Nav.Link as={ NavLink } to="/logout" end>Logout</Nav.Link>
            </Nav>
            </Navbar.Collapse>
                </Container>
              </Navbar>
            )
            
            
    }


    
    
}