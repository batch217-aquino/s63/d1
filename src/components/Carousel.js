import Carousel from 'react-bootstrap/Carousel';

function DarkVariantExample() {
  return (
    <Carousel variant="dark">
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="https://i0.wp.com/communitybynd.com/wp-content/uploads/2022/08/Xperience-HUAWEI-2022.png?fit=1920%2C1080&ssl=1"
          alt="First slide"
        />
        {/* <Carousel.Caption>
          <h5>First slide label</h5>
          <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
        </Carousel.Caption> */}
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="https://www.yugatech.com/wp-content/uploads/2021/12/Infinix-INBook-X1-Pro.jpg"
          alt="Second slide"
        />
        <Carousel.Caption>
          <h5>INBook X1Pro</h5>
          <p>Available in Noble Red, Elves Green, Starfall Grey and Elegant Black. Inspired by the night sky of Trysil on the Norwegian border. These iconic colors pay homage to the natural beauty of Norway’s landscape.

</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="https://www.notebookcheck.net/fileadmin/Notebooks/News/_nc3/Untitled4026.jpg"
          alt="Third slide"
        />
        <Carousel.Caption>
          <h5>Which Apple Watch is right for you?</h5>
          <p>
          Meet the most rugged and capable Apple Watch ever.
          </p>
        </Carousel.Caption>
      </Carousel.Item>
    </Carousel>
  );
}

export default DarkVariantExample;