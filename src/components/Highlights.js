import {Col, Row, Card, Stack} from "react-bootstrap"

export default function Highlights(){
    return(
        <Row className="my-3">
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3">
                    <Card.Body>
                    <Card.Title>Best laptop 2022</Card.Title>
          <Card.Text>
          The best laptops for you, from ultraportables to high-powered editing machines
          </Card.Text>
                    <Stack
            direction="horizontal"
            className="justify-content-between mb-3"
          >
            <img className="w-100" src="https://images.unsplash.com/photo-1605360846332-1378e0c96955?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=404&q=80" alt="img-1" />

          </Stack>
          
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3">
                    <Card.Body>
                    <Card.Title>The future smartphones of 2023/24</Card.Title>
          <Card.Text>
          Here we look at those phones that haven't yet launched, the upcoming phones for 2022 and 2023. 

          </Card.Text>
                    <Stack
            direction="horizontal"
            className="justify-content-between mb-3"
          >
            <img className="w-100" src="https://images.unsplash.com/photo-1644501618169-bab16b6e6efb?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=432&q=80" alt="img-1" />

          </Stack>
          
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3">
                    <Card.Body>
                    <Card.Title>Apple Watch Series 8 and the new Apple Watch SE</Card.Title>
          <Card.Text>
          The redesigned Apple Watch SE delivers the core Apple Watch experience at a new starting price
          </Card.Text>
                    <Stack
            direction="horizontal"
            className="justify-content-between mb-3"
          >
            <img className="w-100" src="https://images.unsplash.com/photo-1542541864-4abf21a55761?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=403&q=80" alt="img-1" />

          </Stack>
          
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    )
    
}