import Banner from "../components/Banner";
import Highlights from "../components/Highlights";
import DarkVariantExample from "../components/Carousel";
import Footer from "../components/Footer";

export default function Home(){
	const data = {
		title: "WELCOME TO TECHSHOP",
		content: "100% Authentic Branded Deals Catch Exclusive Offers Now!",
		destination: "/products",
		label: "Order Now!"
	}
	return(
		<>
			<DarkVariantExample/>
			<Banner bannerProp={data}/>
			<Highlights />
			<Footer/>
		</>
	)
}