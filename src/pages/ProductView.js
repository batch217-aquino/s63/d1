import { useState, useEffect } from "react";

import {Container, Card, Button, Row, Col} from "react-bootstrap";
import { useParams, useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import {
	MDBContainer,
	MDBRow,
	MDBCol,
	MDBCard,
	MDBCardBody,
	MDBCardImage,
	MDBCardTitle,
	MDBIcon,
  } from "mdb-react-ui-kit";
  

export default function ProductView(){
	const { productId } = useParams();

	const navigate = useNavigate();
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);

	useEffect(()=>{
		console.log(productId);

		fetch(`${ process.env.REACT_APP_API_URL }/products/${productId}`)
		.then(res => res.json())
		.then(data => {

			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);

		});

	}, [productId])

	const order = (productId) =>{

		fetch(`${ process.env.REACT_APP_API_URL }/users/order`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
			body: JSON.stringify({
				productId: productId
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);

			if(data){
				Swal.fire({
					title: "Successfully ordered",
					icon: "success",
					text: "You have successfully ordered for this product."
				});

				navigate("/products");
			}
			else{
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				});
			}

		});

	}

	return (
		<MDBContainer fluid className="my-5">
		  <MDBRow className="justify-content-center">
			<MDBCol md="6">
			  <MDBCard className="text-black">
				<MDBIcon fab icon="apple" size="lg" className="px-3 pt-3 pb-2" />
				<MDBCardImage
				  src="https://images.unsplash.com/photo-1413708617479-50918bc877eb?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=873&q=80"
				  position="top"
				  alt="Apple Computer"
				/>
				<MDBCardBody>
				  <div className="text-center">
					<MDBCardTitle>{name}</MDBCardTitle>
					<p className="text-muted mb-4">{description}</p>
				  </div>
				  <div>
					<div className="d-flex justify-content-between">
					  <span>Product Price</span>
					  <span>Php{price}</span>
					</div>
					<div className="d-flex justify-content-between">
					  <span>Shipping fee</span>
					  <span>Php500</span>
					</div>
					<div className="d-flex justify-content-between">
					  <span>Shipping discount</span>
					  <span>Php-500</span>
					</div>
				  </div>
				  <div className="d-flex justify-content-between total font-weight-bold mt-4">
					<span>Total</span>
					<span>Php{price}</span>
				  </div>
				  <div className="m-3 text-center">
				  <Button variant="dark" bg="dark"  size="lg" onClick={() => order(productId)}>Order Now!</Button>

				  </div>
				</MDBCardBody>
			  </MDBCard>
			</MDBCol>
		  </MDBRow>
		</MDBContainer>
	  );
	}
	